<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\URL;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $items = [];
    public function __construct()
    {
        $this->items =
            [
                [
                    'id' => 0,
                    'name' => 'THinh Hung',
                    'user' => 'Hungt'
                ],
                [
                    'id' => 1,
                    'name' => 'Tuan',
                    'user' => 'TuanLD'
                ],
                [
                    'id' => 2,
                    'name' => 'Dung',
                    'user' => 'DungLX'
                ],
                [
                    'id' => 3,
                    'name' => 'Tuan Anh',
                    'user' => 'anhT'
                ],
            ];
    }
    public function index()
    {

        return view('user.index', [
            'title' => 'Danh Sánh',
            'data' => $this->items,
            'topTitle' => 'Danh sách User'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
 
        return view('user.create', [
            'title' => 'Create User',
            'topTitle' => "Tạo Nhân viên",
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $user = $this->items[$id]; 
        return view('user.create', [
            'title' => 'Edit User',
            'topTitle' => "Sửa Nhân viên",
            'user' => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {  
        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


}
