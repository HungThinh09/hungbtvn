<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prefectures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',191);
            $table->string('code',191);
            $table->timestamps();
        });
    }
    // id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prefectures');
    }
};
