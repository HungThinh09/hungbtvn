<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_schedule_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('b_tour_order_id')->nullable();
            $table->unsignedBigInteger('b_tour_order_schedule_id')->nullable();
            $table->date('schedule_date');
            $table->tinyInteger('is_draft_reserved');
            $table->tinyInteger('is_final_reserved');
            $table->text('reserved_content');
            $table->unsignedBigInteger('admin_id');
            $table->string('admin_name', 191);
            $table->timestamps();
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onDelete('cascade');
            // $table->foreign('b_tour_order_schedule_id')->references('id')->on('users')->onDelete('cascade');
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `b_tour_order_id` bigint(20) unsigned NOT NULL,
    // `b_tour_order_schedule_id` bigint(20) unsigned NOT NULL,
    // `schedule_date` date DEFAULT NULL,
    // `is_draft_reserved` tinyint(1) DEFAULT NULL,
    // `is_final_reserved` tinyint(1) DEFAULT NULL,
    // `reserved_content` text COLLATE utf8mb4_unicode_ci,
    // `admin_id` bigint(20) unsigned NOT NULL,
    // `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    // `deleted_at` timestamp NULL DEFAULT NULL,
    // PRIMARY KEY (`id`),
    // KEY `b_schedule_histories_b_tour_order_schedule_id_foreign` (`b_tour_order_schedule_id`),
    // KEY `b_schedule_histories_admin_id_foreign` (`admin_id`),
    // CONSTRAINT `b_schedule_histories_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_schedule_histories');
    }
};
