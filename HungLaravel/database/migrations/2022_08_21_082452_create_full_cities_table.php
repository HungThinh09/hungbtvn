<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('full_cities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('prefecture');
            $table->string('name', 191);
            $table->string('code', 191);
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('full_cities');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
};
