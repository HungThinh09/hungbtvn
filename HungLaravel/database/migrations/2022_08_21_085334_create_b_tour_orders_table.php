<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 191);
            $table->bigInteger('price');
            $table->string('tourname_name', 191);
            $table->text('tourname_explain');
            $table->string('tourname_image', 191);
            $table->text('edit_history');
            $table->text('description');
            $table->json('concept');
            $table->json('budget');
            $table->json('note');
            $table->text('contact_content');
            $table->text('tourname_image_thumbnail');
            $table->string('public_url', 500);
            $table->text('tourname_image_preview');
            $table->text('contact_image_thumbnail');
            $table->tinyInteger('type');
            $table->unsignedBigInteger('furusato_tour_city_id');
            $table->string('extra_name', 191);
            $table->string('extra_image', 191);
            $table->string('tour_image_user', 191);
            $table->timestamps();
        });
    }
    //     `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    //   `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `price` bigint(20) unsigned NOT NULL,
    //   `tourname_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `tourname_explain` text COLLATE utf8mb4_unicode_ci,
    //   `tourname_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `edit_history` text COLLATE utf8mb4_unicode_ci,
    //   `description` text COLLATE utf8mb4_unicode_ci,
    //   `concept` json DEFAULT NULL,
    //   `budget` json DEFAULT NULL,
    //   `note` json DEFAULT NULL,
    //   `contact_content` text COLLATE utf8mb4_unicode_ci,
    //   `contact_image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `public_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `created_at` timestamp NULL DEFAULT NULL,
    //   `updated_at` timestamp NULL DEFAULT NULL,
    //   `tourname_image_preview` text COLLATE utf8mb4_unicode_ci,
    //   `tourname_image_thumbnail` text COLLATE utf8mb4_unicode_ci,
    //   `contact_image_preview` text COLLATE utf8mb4_unicode_ci,
    //   `contact_image_thumbnail` text COLLATE utf8mb4_unicode_ci,
    //   `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: Normal, 2: Furusato.',
    //   `furusato_tour_city_id` bigint(20) unsigned DEFAULT NULL,
    //   `furusato_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `furusato_from_site` text COLLATE utf8mb4_unicode_ci,
    //   `extra_info` json DEFAULT NULL,
    //   `concept_user` json DEFAULT NULL,
    //   `budget_user` json DEFAULT NULL,
    //   `note_user` json DEFAULT NULL,
    //   `contact_content_user` text COLLATE utf8mb4_unicode_ci,
    //   `contact_image_user` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `tour_name_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `tour_explain_user` text COLLATE utf8mb4_unicode_ci,
    //   `tour_image_user` text COLLATE utf8mb4_unicode_ci,
    //   `extra_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    //   `extra_image` text COLLATE utf8mb4_unicode_ci,

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_tour_orders');
    }
};
