<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_code', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('local_code',191);
            $table->string('old_zip_code',191);
            $table->string('zip_code',191);
            $table->string('prefecture_name_kata',191);
            $table->string('city_name_kata',191);
            $table->string('town_name_kata',191);
            $table->string('prefecture_name_kanji',191);
            $table->tinyInteger('display_multiple_zip_code');
            $table->integer('display_small_letter');
            $table->integer('display_town_area');
            $table->integer('display_multiple_town_area');
            $table->integer('display_update');
            $table->timestamps();
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `local_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `old_zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `prefecture_name_kata` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `city_name_kata` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `town_name_kata` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `prefecture_name_kanji` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `city_name_kanji` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `town_name_kanji` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `display_multiple_zip_code` int(11) NOT NULL,
    // `display_small_letter` int(11) NOT NULL,
    // `display_town_area` int(11) NOT NULL,
    // `display_multiple_town_area` int(11) NOT NULL,
    // `display_update` int(11) NOT NULL,
    // `display_reason_change` int(11) NOT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    // PRIMARY KEY (`id`)
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_code');
    }
};
