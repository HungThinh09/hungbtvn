<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sei', 191);
            $table->string('mei', 191);
            $table->string('sei_kana', 191);
            $table->string('mei_kana', 191);
            $table->string('phone', 191);
            $table->string('email', 191);
            $table->date('birthday',191);
            $table->string('postal_code',191);
            $table->string('address',191);
            $table->bigInteger('prefecture_id');
            $table->unsignedBigInteger('city_id');
            $table->string('stress',191);
            $table->string('building',191);
            $table->string('user_type',191);
            $table->tinyInteger('active');
            $table->tinyInteger('gender');
            $table->string('introduce_name',191);
            $table->timestamp('email_verified_at');
            $table->string('password',191);
            $table->string('tmp_password',191);
            $table->dateTime('last_login');
            $table->integer('login_fails');
            $table->dateTime('last_login_fail_at');
            $table->string('remenber_token',100);
            $table->timestamp('delete_at');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('members');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
};
