<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',191);
            $table->text('content');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('notification_id');
            $table->unsignedBigInteger('booking_id');
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
    // `user_id` bigint(20) unsigned DEFAULT NULL,
    // `notification_id` bigint(20) unsigned DEFAULT NULL,
    // `booking_id` bigint(20) unsigned DEFAULT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    // PRIMARY KEY (`id`),
    // KEY `notification_users_user_id_foreign` (`user_id`),
    // CONSTRAINT `notification_users_user_id_foreign` FOREIGN KEY (`user_id`) 
    // REFERENCES `users` (`id`) ON DELETE CASCADE
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_users');
    }
};
