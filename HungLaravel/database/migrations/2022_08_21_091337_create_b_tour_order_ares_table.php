<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_order_ares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('prefecture_id');
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('b_tour_order_id');
            $table->smallInteger('order_no');
            $table->timestamps();
            $table->foreign('prefecture_id')->references('id')->on('prefectures')->onDelete('cascade');
            $table->foreign('city_id')-> references('id')->on('full_cities')->onDelete('cascade');
            $table->foreign('b_tour_order_id') -> references('id')->on('b_tour_orders')->onDelete('cascade');
        });
    }
//   $table->integerIncrements('id');
//             $table->unsignedBigInteger('prefecture_id',191);
//             $table->unsignedBigInteger('city_id',191);
//             $table->unsignedBigInteger('b_tour_order_id',191);
//             $table->smallInteger('order_no');
//             $table->timestamps();
            // $table->foreign('prefecture_id')->references('id')->on('prefectures')->onDelete('cascade');
            // $table->foreign('city_id')-> references('id')->on('full_cities')->onDelete('cascade');
            // $table->foreign('b_tour_order_id') -> references('id')->on('b_tour_orders')->onDelete('cascade');
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('b_tour_order_ares');
    }
};

