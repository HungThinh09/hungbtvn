<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_id');
            $table->string('company_name',191);
            $table->string('office_name',191);
            $table->string('map_url',500);
            $table->string('manager_email',500);
            $table->string('tel',191);
            $table->mediumInteger('status');
            $table->string('start_work',191);
            $table->string('end_work',191);
            $table->string('description',2000);
            $table->string('image',500);
            $table->timestamps();
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `category_id` bigint(20) unsigned NOT NULL,
    // `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `office_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `map_url` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `homepage_url` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `manager_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `tel` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `status` mediumint(8) unsigned NOT NULL,
    // `start_work` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `end_work` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `image` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    // PRIMARY KEY (`id`)
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offices');
    }
};
