<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('b_schedule_histories', function (Blueprint $table) {
              $table->foreign('b_tour_order_schedule_id')->references('id')
            ->on('b_tour_order_schedules')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('b_schedule_histories', function (Blueprint $table) {
            //
        });
    }
};
