<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('member_id');
            $table->unsignedBigInteger('b_tour_order_id');
            $table->unsignedBigInteger('tour_order_template_id');
            $table->unsignedBigInteger('admin_id');
            $table->integer('discuss_method');
            $table->json('discuss_time');
            $table->date('hearing_first_time');
            $table->text('hearing_tour_purpose');
            $table->string('hearing_budget',191);
            $table->string('hearing_plan_time',191);
            $table->string('hearing_area',191);
            $table->unsignedBigInteger('adult_count');
            $table->unsignedBigInteger('child_count');
            $table->unsignedBigInteger('payment_confirm_admin_id');
            $table->tinyInteger('is_receive_info');
            $table->unsignedTinyInteger('type')->comment('1:Normal, 2:Furusato')->default(1);
            $table->text('note');
            $table->timestamps();
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onDelete('cascade');
            $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');

            
        });
    }
//     KEY `bookings_member_id_foreign` (`member_id`),
//   KEY `bookings_b_tour_order_id_foreign` (`b_tour_order_id`),
//   KEY `bookings_admin_id_foreign` (`admin_id`),
//   KEY `bookings_payment_confirm_admin_id_foreign` (`payment_confirm_admin_id`),
//   KEY `bookings_tour_order_template_id_foreign` (`tour_order_template_id`),
//   CONSTRAINT `bookings_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
//   CONSTRAINT `bookings_b_tour_order_id_foreign` FOREIGN KEY (`b_tour_order_id`) REFERENCES `b_tour_orders` (`id`) ON DELETE CASCADE,
//   CONSTRAINT `bookings_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE,
//   CONSTRAINT `bookings_payment_confirm_admin_id_foreign` FOREIGN KEY (`payment_confirm_admin_id`) REFERENCES `users` (`id`)
//     `payment_confirm_admin_id` bigint(20) unsigned DEFAULT NULL,
//   `is_receive_info` tinyint(1) DEFAULT NULL,
//   `type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1: Normal, 2: Furusato.',
//   `note` text COLLATE utf8mb4_unicode_ci,
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `member_id` bigint(20) unsigned NOT NULL,
    // `b_tour_order_id` bigint(20) unsigned DEFAULT NULL,
    // `tour_order_template_id` bigint(20) unsigned DEFAULT NULL,
    // `admin_id` bigint(20) unsigned DEFAULT NULL,
    // `discuss_method` int(11) DEFAULT NULL,
    // `discuss_time` json DEFAULT NULL,
    // `hearing_first_time` date DEFAULT NULL,
    // `hearing_tour_purpose` text COLLATE utf8mb4_unicode_ci,
    // `hearing_budget` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `hearing_plan_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `hearing_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `adult_count` bigint(20) unsigned DEFAULT NULL,
    // `child_count` bigint(20) unsigned DEFAULT NULL,
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
};
