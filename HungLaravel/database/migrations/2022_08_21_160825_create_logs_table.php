<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('object_id');
            $table->unsignedBigInteger('owner_id');
            $table->integer('category');
            $table->integer('action');
            $table->text('info');
            $table->mediumText('before');
            $table->mediumText('before_changes');
            $table->mediumText('after');
            $table->mediumText('after_changes');
            $table->timestamps();
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `object_id` bigint(20) unsigned NOT NULL,
    // `owner_id` bigint(20) unsigned DEFAULT NULL,
    // `category` int(11) NOT NULL,
    // `action` int(11) NOT NULL,
    // `info` text COLLATE utf8mb4_unicode_ci,
    // `before` mediumtext COLLATE utf8mb4_unicode_ci,
    // `before_changes` mediumtext COLLATE utf8mb4_unicode_ci,
    // `after` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
    // `after_changes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
};
