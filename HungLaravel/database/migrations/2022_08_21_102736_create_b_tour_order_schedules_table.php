<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('b_tour_order_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('b_tour_order_id');
            $table->string('title',191);
            $table->string('description',191);
            $table->json('detail',191);
            $table->date('date');
            $table->timestamps();
            $table->foreign('b_tour_order_id')->references('id')->on('b_tour_orders')->onDelete('cascade');
            // $table->foreign('id')->references('b_tour_order_schedule_id')
            // ->on('b_schedule_histories')->onDelete('cascade');
           
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `b_tour_order_id` bigint(20) unsigned NOT NULL,
    // `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `date` date DEFAULT NULL,
    // `detail` json DEFAULT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    // PRIMARY KEY (`id`),
    // KEY `b_tour_order_schedules_b_tour_order_id_foreign` (`b_tour_order_id`),
    // CONSTRAINT `b_tour_order_schedules_b_tour_order_id_foreign` FOREIGN KEY (`b_tour_order_id`) 
    // REFERENCES `b_tour_orders` (`id`) ON DELETE CASCADE
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('b_tour_order_schedules');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
       
    }
};
