<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_templates', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('tour_order_template_id')->nullable();
            $table->string('title',191)->default(null);
            $table->string('description',191)->default(null);
            $table->date('date')->default(null);
            $table->json('detail')->default(null);
            $table->timestamps();
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `tour_order_template_id` bigint(20) unsigned NOT NULL,
    // `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `date` date DEFAULT NULL,
    // `detail` json DEFAULT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    // PRIMARY KEY (`id`),
    // KEY `schedule_templates_tour_order_template_id_foreign` (`tour_order_template_id`),
    // CONSTRAINT `schedule_templates_tour_order_template_id_foreign` FOREIGN KEY (`tour_order_template_id`) 
    // REFERENCES `tour_order_templates` (`id`) ON DELETE CASCADE
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_templates');
    }
};
