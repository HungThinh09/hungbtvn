<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('member_id');
            $table->unsignedInteger('booking_id');
            $table->unsignedInteger('admin_id');
            $table->double('amount');
            $table->string('paypal_payment_id',191);
            $table->string('token',191);
            $table->string('payer_id',191);
            $table->timestamp('deleted_at');
            $table->timestamps();
            // $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
            // $table->foreign('booking_id')->references('id')->on('bookings')->onDelete('cascade');
            // $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');
        });
    }
    // `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
    // `member_id` bigint(20) unsigned NOT NULL,
    // `booking_id` bigint(20) unsigned NOT NULL,
    // `admin_id` bigint(20) unsigned DEFAULT NULL,
    // `amount` double NOT NULL,
    // `status` tinyint(4) NOT NULL,
    // `paypal_payment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `payer_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    // `created_at` timestamp NULL DEFAULT NULL,
    // `updated_at` timestamp NULL DEFAULT NULL,
    // `deleted_at` timestamp NULL DEFAULT NULL,
    // PRIMARY KEY (`id`),
    // KEY `history_payments_member_id_foreign` (`member_id`),
    // KEY `history_payments_booking_id_foreign` (`booking_id`),
    // KEY `history_payments_admin_id_foreign` (`admin_id`),
    // CONSTRAINT `history_payments_admin_id_foreign` FOREIGN KEY (`admin_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
    // CONSTRAINT `history_payments_booking_id_foreign` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`id`) ON DELETE CASCADE,
    // CONSTRAINT `history_payments_member_id_foreign` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_payments');
    }
};
