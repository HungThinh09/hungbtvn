<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\department>
 */
class departmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'prority' => $this->faker->numberBetween($min = 0, $max = 5),
            'email' => $this->faker->safeEmail(),
            'updated_at' => $this->faker->date("Y-m-d H:i:s"),
            'created_at' => $this->faker->date("Y-m-d H:i:s")
        ];
    }
}
