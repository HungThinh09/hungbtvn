<div>
    <button type="submit" name="{{ $name ?? '' }}" class="btn btn-primary">{{ $nameButton ?? 'button' }}</button>
</div>
