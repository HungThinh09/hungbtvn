<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    @yield('css')
</head>

<body>
    <div class="container">
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <div class="container-fluid">
                    <a class="navbar-brand" href="#">HungT</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link {{ url()->full() == 'https://hunglaravel.dev/user' ? 'active' : '' }}"
                                    href="{{ route('user.index') }}">Danh sách</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link  {{ url()->full() == 'https://hunglaravel.dev/user/create' ? 'active' : '' }}"
                                    href="{{ route('user.create') }}">Tạo User</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link  {{ isset($user['id']) && url()->full() == route('user.edit', $user['id']) ? 'active' : '' }}"
                                    href="#">Update User</a>
                            </li>

                    </div>
                </div>
            </nav>
        </header>
        <main>
            <div class="content" style="padding: 20px 0">
                <div>
                    <h2>{{ $topTitle ?? 'Trang chủ' }}</h2>
                </div>
                @yield('main')
            </div>
        </main>
    </div>
    <footer>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
            integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
        </script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
            integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
        </script>
    </footer>
</body>

</html>
