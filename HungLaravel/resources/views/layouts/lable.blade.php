<div class="form-group">
    <label for="">{{ $title ?? ' ' }}</label>
    <input type="{{$type ?? ''}}" name="{{ $name ?? '' }}" value="{{ $value ?? '' }}" class="form-control"
        placeholder="Mời nhập {{ $name ?? '' }}">
</div>
