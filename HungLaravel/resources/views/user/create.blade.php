@extends('layouts.main')
@section('main')
    <div>
        <a href="{{ route('user.index') }}" class="btn btn-success">Danh sách Nhân viên</a>
        @if (isset($user))
            <form action="{{route('user.update', $user['id'])}}" method="post">
                @csrf
                @method('put')
                @include('layouts.lable', ['title' => 'Name', 'name' => 'name', 'value' => $user['name']])
                @include('layouts.lable', ['title' => 'User', 'name' => 'user', 'value' => $user['user']])
                @include('layouts.button', ['nameButton' => 'Sửa nhân viên', 'name' => 'userEdit'])
            </form>
        @else
            <form action="" method="post">
                @csrf
                @method('put')
                @include('layouts.lable', ['title' => 'Name', 'name' => 'name'])
                @include('layouts.lable', ['title' => 'User', 'name' => 'user'])
                @include('layouts.button', ['nameButton' => 'Tạo nhân viên', 'name' => 'userAdd'])
            </form>
        @endif

    </div>
@endsection
