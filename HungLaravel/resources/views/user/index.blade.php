@extends('layouts.main')
@section('main')
    <div>
        <a href="{{route('user.create')}} " class="btn btn-primary">Tạo User</a>
        <table class="table table-light">
            <thead class="thead-light"> 
                <tr>
                    <th>STT</th>
                    <th>TÊN</th>
                    <th>USER</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $key => $user)
                <tr>
                    <td>{{ $key +1}}</td>
                    <td>{{ $user['name'] }}</td>
                    <td>{{ $user['user'] }}</td>
                    <td>
                    <a href="{{ route('user.edit', $user['id']) }}" class="btn btn-warning">Edit</a>
                    <a href="#" class="btn btn-danger">DELETE</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
