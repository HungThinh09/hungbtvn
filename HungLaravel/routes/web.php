<?php

use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('user', UserController::class);
// Route::resource('test', UserController::class);
// route::prefix('admin')->group(function () {
//     // Route::resource('user', UserController::class);
//     // Route::resource('department', DepartmentController::class);
//     // Route::resource('position', PositionController::class);
//     // Route::resource('role', RoleController::class);
//     // Route::resource('changePass', ChangePassController::class);
// });
 

route::get('admin', [AdminController::class, 'index'])->name('admin');
// route::get('admin/create', [AdminController::class, 'create'])->name('create');